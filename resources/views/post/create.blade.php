<!-- create.blade.php -->

@extends('layouts.default')
@section('content')
<div class="container" style="margin-top: 40px;">

  <form method="post" action="{{url('admin/post')}}" enctype="multipart/form-data">
    <div class="form-group row">
    {{csrf_field()}}
      <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
      <div class="col-sm-10">
        <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="title" name="title">
      </div>
    </div>
    <div class="form-group row">
      <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Post</label>
      <div class="col-sm-10">
        <textarea name="post" rows="8" cols="80"></textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Preview Image</label>
      <div class="col-sm-10">
        <input type="file" name="photo" class="form-control form-control-lg">
      </div>
    </div>
    <div class="form-group row">
      <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Caption</label>
      <div class="col-sm-10">
        <input type="text" name="img_caption"></textarea>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-md-2"></div>
      <input type="submit" class="btn btn-primary">
      <a class="btn btn-default btn-close" href="{{ URL::route('home') }}">Cancel</a>
    </div>
    <input type="hidden" name="user_id" value="1">
  </form>
</div>
@endsection