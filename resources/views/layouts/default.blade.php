<!-- default.blade.php -->
<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=1095666640450151";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

    <header>
        <div class="container">
            @include('includes.header')
        </div>
    </header>

    <div id="main" class="row">
        @yield('content')
    </div>

    <footer class="row">
        @include('includes.footer')
    </footer>
    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $('#oc-posts').owlCarousel({
                items: 3
              });

          $('.owl-carousel').owlCarousel({
            items: 6
          });
        });
    </script>

</body>
</html>