@extends('layouts.default')
@section('content')
<div class="container detail-container">
    <!-- Page Title
	============================================= -->
	<section id="page-title">

		<div class="breadcrumb-menu">
			<ol class="breadcrumb">
				<li><a href="#">Blog</a></li>
				<li class="active">{{$post->title}}</li>
			</ol>
			<div class="title">Lorem ipsum dolor sit amet, onsectetur adipiscing elit semper, tortor nec</div>
		</div>

	</section><!-- #page-title end -->

	<section id="content">
	<div class="">
		<div class="entry clearfix">
			<!-- Entry Meta ============================================= -->

			<ul class="entry-meta clearfix" >
				<li><a href="#"> {{$post->user->name}} </a></li>
				<li> {{date('d M Y', strtotime($post->created_at))}} </li>
				<li><a href="#"> WWF </a></li>
				
			</ul><!-- .entry-meta end -->
		</div>

		<div class="list-group">
			<ul class="category-meta clearfix" >
				<li class="first">Topics</li>
				<li class="second">Valuing Ecosystem Services</li>
				<li class="active">Reuse and Recycling</li>
				<li class="second">REDD++</li>
				
			</ul><!-- .entry-meta end -->
		</div>

		<style type="text/css">
			.entry-meta {
				list-style: none;
				margin: 10px -10px -15px 0;
				margin-bottom: 20px;
				display: block;
				padding: 0;
			}
			.entry-meta li {
				float: left;
				font-size: 12px;
				line-height: 14px;
				font-family: "Open Sans";
			}

			.entry-meta li:after {
				content: "/ ";
			}

			.category-meta {
				list-style: none;
				margin: 10px -10px -15px 0;
				display: block;
				padding: 0;
				margin-bottom: 30px;
			}
			.category-meta li {
				float: left;
				font-size: 12px;
				line-height: 14px;
				font-family: "Open Sans";
			}

			.category-meta .first {
				font-weight: bold;
				color: #636363;
				padding: 5px 10px 5px 10px;
				margin-right: 5px;
			}

			.category-meta .second {
				color: #636363;
				padding: 5px 10px 5px 10px;
				background-color: #c2c2c2;
				margin-right: 5px;
			}

			.category-meta .active {
				color: #fc8e2e;
				padding: 5px 10px 5px 10px;
				border-color: #fc8e2e;	
				border: 1px solid;
				margin-right: 5px;
			}

		</style>

		<div class="post-content">
			<div class="opening-text">
				Marem ipsum dolor sit amet, constectur adipiscing elit. Donec semper, tortor nec dictum tincidunt, orci tortor porttitor diam, a viverra felis orci vitae lacus.
			</div>
			<h1>{{$post->title}}</h1>
			<p>{{$post->post}}</p>
			@if ($post->preview_img)
			<div class="entry-image">
				<a href="#"><img src="{{ URL::asset('uploads/')}}/{{$post->preview_img}}" alt="Blog Single"></a>
				@if ($post->img_caption)
				<span class="caption-image">{{ $post->img_caption }}</span>
				@endif
			</div>
			@endif
			<p>{{$post->post}}</p>
			<!-- <blockquote><p>{{$post->post}}</p></blockquote> -->
			<blockquote><p>
				Sed dignissim, leo at gravida facilisis, magna erat consequat augue, vitae viverra, neque nunc eu ligula. Praesent vel gravida urna.
			</p></blockquote>
			<p>{{$post->post}}</p>
		</div>
		
		<div class="share">
			<span>share on</span>
			<div class='social-group'>
			  <div class='icon social fb'><i class='fa fa-facebook'></i></div>
			  <div class='icon social tw'><i class='fa fa-twitter'></i></div>
			  <div class='icon social gp'><i class='fa fa-google-plus'></i></div>
			  <div class='icon social en'><i class='fa fa-envelope'></i></div>
			  <div class='icon social rs'><i class='fa fa-rss'></i></div>
			</div>  
		</div>
		<!-- Post Author Info -->
		
		<div class="panel-user">
			<div class="author-image">
				<img src="{{ URL::asset('images/photo-16.jpg')}}" alt="" class="img-circle">
			</div>
			<div class="panel-heading">
				<div class="panel-title">
					<span>{{$post->user->name}}</span>
				</div>
				<div class="author-role">Public Relations, WWF</div>
			</div>
			<div class="panel-content">
			<p>Curabitur ut elit libero. Donec nisl ex, fringilla in ullamcorper at, fermentum ac erat. Praesent ullamcorper ullamcorper nunc sed bibendum. Duis in tellus a lectus gravida accumsan</p>
			</div>
		</div>
		
		<style type="text/css">
			.panel-user {
				border-top: 1px solid #c2c2c2;
				margin-bottom: 60px;
				padding-top: 40px;
			}

			.panel-user .author-image {
				float: left;
				height: 150px;
				min-width: 100px;
				padding-right: 25px;
			}

			.panel-user .panel-title {
				color: #252525;
				font-weight: bold;
			}

			blockquote {
				border: none;
				border-top: 1px solid #c2c2c2;	
				border-bottom: 1px solid #c2c2c2;
				margin-top: 40px;
				margin-bottom: 40px;
			}

			blockquote p {
				text-align: center;
				font-style: italic;
				font-family: sans-serif;
				font-size: 30px;
				padding-bottom: 10px;
				padding-top: 10px;
				color: deepskyblue;
			}

			.detail-container {
				width: 960px;
				font-family: "Montserrat";
			}

			.detail-container .title {
				color: #252525;
				font-size: 40px;
				line-height: 1.1;
				margin-bottom: 5px;
			}

			.breadcrumb-menu {
				margin-top: 30px;
				font-size: 12px;
			}

			.breadcrumb-menu ol {
				padding-left: 0;
				background: none; 
			}

			.opening-text {
				font-weight: bold;
				color: #252525;
				font-size: 20px;
				line-height: 1.5;
				margin-bottom: 30px;
			}

			h1 {
				text-transform: uppercase;
				color: #252525;
				font-size: 18px;
				font-weight: bold;
			}

			.entry-image {
				text-align: center;
				margin-top: 40px;
				margin-bottom: 60px;
			}

			.entry-image a img {
				width: 960px;
			}

			.entry-image .caption-image {
				float: left;
				padding-top: 5px;
				font-size: 11px;
			}

			.post-content {
				margin-bottom: 40px;
			}

			.share	{
				text-transform: uppercase; 
				margin-bottom: 40px;
				font-weight: bold;
				padding-bottom: 30px;
			}

			.demopadding {
			  margin:50px auto;
			  width:140px;
			  text-align:center;
			}
			.icon {
				position:relative;
				text-align:center;
				width:0px;
				height:0px;
				padding:20px;
				border-top-right-radius: 	20px;
				border-top-left-radius: 	20px;
				border-bottom-right-radius: 20px;
				border-bottom-left-radius: 	20px; 
				-moz-border-radius: 		20px 20px 20px 20px;
				-webkit-border-radius: 		20px 20px 20px 20px;
				-khtml-border-radius: 		20px 20px 20px 20px; 	
				color:#FFFFFF;
			}
			.icon i {
				font-size:20px;
				position:absolute;
				left:9px;
				top:10px;
			}
			.icon.social {
				float:left;
				margin:0 5px 0 0;
				cursor:pointer;
				background:#fff ;
				color:#262626;
				transition: 0.5s;
				-moz-transition: 0.5s;
				-webkit-transition: 0.5s;
				-o-transition: 0.5s; 	
			}
			.icon.social:hover {
				background:#262626 ;
				color:#6d6e71;
				transition: 0.5s;
				-moz-transition: 0.5s;
				-webkit-transition: 0.5s;
				-o-transition: 0.5s;
				-webkit-filter:	drop-shadow(0 1px 10px rgba(0,0,0,.8));
				-moz-filter: 	drop-shadow(0 1px 10px rgba(0,0,0,.8));
				-ms-filter: 		drop-shadow(0 1px 10px rgba(0,0,0,.8));
				-o-filter: 		drop-shadow(0 1px 10px rgba(0,0,0,.8));
				filter: 			drop-shadow(0 1px 10px rgba(0,0,0,.8));	 	
			}
			.icon.social.fb i {
				left:13px;
				top:10px;
				color: #007caf;
			}
			.icon.social.tw i {
				left:11px;
				color: #00b0e4;
			}
			.icon.social.gp i {
				left:11px;
				color: #dd4634;
			}
			.icon.social.ma i {
				left:11px;
				color: #636363;
			}
			.icon.social.rs i {
				left:11px;
				color: #f97c00;	
			}

		</style>
	</div>
	</section>
</div>
	
@stop