@extends('layouts.default')
@section('content')
	<div class="container">
        <nav class="navbar navbar-default submenu" style="margin-top: 20px;margin-bottom: 20px;">
            <ul class="nav navbar-nav">
                <li><a href="/">RSPO</a></li>
                <li><a href="/">WWF International</a></li>
                <li><a href="/">WWF Tigers Alive Initiative</a></li>
                <li><a href="/">World Resources Institute</a></li>
                <li><a href="/">The Coral Triangle</a></li>
            </ul>
        </nav>    
    </div>

    <style type="text/css">
        .submenu .navbar-nav li {
            margin-right: 40px;
        }

        .submenu .navbar-nav li a {
            font-size: 13px;
            color: #636363;
        }

    </style>

    <div class="features-big">
        <div class="features-left">
            <div class="feature-item" style="background-image: url({{ URL::asset('images/photo-01.jpg') }});">
                <a href="#">
                    <div class="text-wrap">
                        <h2 class="title">Lorem Ipsum</h2>
                        <p class="excerpt">Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="features-right">
            <div class="feature-item" style="background-image: url({{ URL::asset('images/photo-02.jpg') }});">
                <a href="#">
                    <div class="text-wrap">
                        <h2 class="title">Lorem Ipsum</h2>
                    </div>
                </a>
            </div>
            <div class="feature-item" style="background-image: url({{ URL::asset('images/photo-03.jpg') }});">
                <a href="#">
                    <div class="text-wrap">
                        <h2 class="title">Lorem Ipsum</h2>
                    </div>
                </a>
            </div>
        </div>
    </div>



    <!-- Container for post slider -->
    <div class="container-posts">

        <!-- <div class="fancy-title title-center title-dotted-border topmargin">
            <h3>Posts Carousel</h3>
        </div> -->
        <div class="container">
            <div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="4">
            @if ($posts)
                <?php $index = 0;?>
                @foreach ($posts as $post)
                

                @if ($index % 2 == 0)
                <div class="row">
                @endif

                    <div class="oc-item-post">
                        <div class="ipost clearfix">
                            <div class="entry-image">
                                <a href="{{ URL::route('detail', $post['id'])}}"><img class="image_fade" src="{{ URL::asset('uploads/')}}/{{$post['preview_img']}}"></a>
                            </div>
                            <div class="entry-meta">
                                <span class="entry-meta clearfix">{{date('d M Y', strtotime($post['created_at']))}}</span>
                            </div>
                            <div class="entry-title">
                                <a href="blog-single.html">{{$post['title']}}</a>
                            </div>
                            
                        </div>
                    </div>

                <?php $index++;?>

                @if ($index % 2 == 0)
                </div>
                @endif
                
                @endforeach
            @endif

            </div>
            
        </div>

        <div class="clear"></div>
    </div>

    <!-- end of slider -->
    <div class="full-container clearfix">
        <div class="container list-client">
        <!-- Clients -->
        <div class="" style="text-align: center;margin-bottom: 40px;margin-top: 50px">
            <h3>Clients</h3>
        </div>
        <div id="oc-clients" class="owl-carousel owl-theme owl-wrap">

            <div class="oc-item"><a href="#"><img src="{{ URL::asset('images/Akoma Logo.png')  }}"></a></div>
            <div class="oc-item"><a href="#"><img src="{{ URL::asset('images/Thorntons Logo.png')  }}"></a></div>
            <div class="oc-item"><a href="#"><img src="{{ URL::asset('images/Vieira Logo.png')  }}"></a></div>
            <div class="oc-item"><a href="#"><img src="{{ URL::asset('images/Arfuels Logo.png')  }}"></a></div>
            <div class="oc-item"><a href="#"><img src="{{ URL::asset('images/Erca Logo.png')  }}"></a></div>
            <div class="oc-item"><a href="#"><img src="{{ URL::asset('images/Rose logo.png')  }}"></a></div>

        </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="container" style="margin-top: 40px;margin-bottom: 40px">
        <div class="row">
            <div class="col-sm-4"> 
                <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-width="330" data-height="580" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
            </div>
            <div class="col-sm-4">
                <a class="twitter-timeline" href="https://twitter.com/catalyzecomms"></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
            <div class="col-sm-4"></div>
            
        </div>
    </div>
    <div class="clear"></div>

    <style type="text/css">
        .features-big {
            width: 100%;
            height: 400px;
        }
        .features-big .features-left,
        .features-big .features-right {
            height: 400px;
            display: block;
            float: left;
        }
        .features-big .feature-item {
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        .features-big .feature-item > a {
            width: 100%;
            height: 100%;
            display: flex;
            text-decoration: none;
            color: #FFFFFF;
            align-items: flex-end;
        }
        .features-big .features-left {
            width: 60%;
        }
        .features-big .features-left .feature-item {
            height: 100%;
            padding: 30px;
        }
        .features-big .feature-item > a .text-wrap {
            width: 100%;
        }
        .features-big .features-left .feature-item > a h2 {
            font-size: 24px;
            font-weight: bold;
            width: 100%;
            margin: 0;
            line-height: 1.4;
            display: block;
        }
        .features-big .features-left .feature-item > a p {
            font-size: 15px;
            width: 100%;
            margin-top: 20px;
            margin-bottom: 0;
            display: block;
            line-height: 1.5;
        }
        .features-big .features-right {
            width: 40%;
        }
        .features-big .features-right .feature-item {
            height: 50%;
            width: 100%;
            padding: 20px;
            float: left;
        }
        .features-big .features-right .feature-item > a h2 {
            font-size: 18px;
            font-weight: bold;
            width: 100%;
            margin: 0;
            line-height: 1.4;
            display: block;
        }
        .full-container {
            border-bottom: 1px solid #c2c2c2;
        }
    </style>
    

@stop