<!-- Footer
============================================= -->
<footer id="footer" class="grey">

	<div class="container">
		<div class="row" style="margin-top: 60px;margin-bottom: 60px;">
			<div class="footer-container">
				<!-- About -->
				<div class="footer-title">
					<h4>About</h4>	
				</div>
				<ul class="list-unstyled footer-list">
					<li>Home</li>
					<li>About Us</li>
					<li>Contact</li>
					<li>Social Media</li>
					<li>Blog</li>
				</ul>
			</div>
			<div class="footer-container">
				<!-- Works -->		
				<div class="footer-title">
					<h4>Works</h4>
				</div>
				<ul class="list-unstyled footer-list">
					<li>Forest Solutions</li>
					<li>Soy Scorecards</li>
					<li>Palm Oil Scorecards</li>
					<li>School of Fist</li>
					<li>RSPO</li>
				</ul>
			</div>		
			<div class="footer-container">
				<!-- Clients -->
				<div class="footer-title">
					<h4>Clients</h4>
				</div>
				<ul class="list-unstyled footer-list">
					<li>WWF International</li>
					<li>RSPO</li>
					<li>Lorem Ipsum</li>
					<li>Dolor si</li>
					<li>Amet</li>
				</ul>
			</div>	
			<div class="contact-us-container">	
				<!-- Contact Us -->
				<div class="footer-title"><h4>Contact Us</h4></div>
				<p>Ut tristique non elit nec accusman. Nunc ullamcorper metus at dui luctus, non cursus odio scelerisque. Nulla imperdient varius arcu quis faucibus. Sed at consectetur lorem.</p>
				<form class="contact-us-form">
					<input type="text" placeholder="Message">
					<input type="text" class="email" placeholder="Your email address">
					<input type="submit">
				</form>
			</div>
		</div>
	</div>
	<style type="text/css">
		h4 {
			text-transform: uppercase;
			font-size: 12px;
			color: #252525;
			padding-bottom: 20px;
		}

		.footer-title {
			margin-bottom: 20px;
			border-bottom: 1px solid #c2c2c2;
		}

		.footer-column {
			margin-right: 40px;
		}

		.footer-container {
			width: 180px;
			float: left;
			margin-right: 40px;
		}

		.contact-us-container {
			float: left;
			width: 410px;
			display: block;
		}

		.footer-container .footer-list li {
			padding-bottom: 30px;
			font-family: "Open Sans";
			font-size: 12px;
			color: #898989;
		}

		.contact-us-container p {
			font-family: "Open Sans";
			font-size: 12px;
			color: #898989;
		}

		.contact-us-form {
			margin-top: 20px;
			font-size: 14px;
		}

		.contact-us-form input {
			border: none;
		}		

		.contact-us-form input[type="text"] {
			padding-left: 15px;
			width: 100%;
			height: 50px;
			color: #636363;
			border-bottom: 1px solid #c2c2c2;
		}

		.contact-us-form input[class="email"] {
			width: 70%;
			float: left;
		}

		.contact-us-form input[type="submit"] {
			width: 30%;
			height: 50px;
			font-weight: bold;
			color: #FFFFFF;
			background-color: #252525;
			text-transform: uppercase;
		}
	</style>
</footer><!-- #footer end -->