<div class="container submenu">
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li><a href="/">RSPO</a></li>
            <li><a href="/">WWF International</a></li>
            <li><a href="/">WWF Tigers Alive Initiative</a></li>
            <li><a href="/">World Resources Institute</a></li>
            <li><a href="/">The Coral Triangle</a></li>
        </ul>
    </nav>    
</div>