<div class="top-header">
	<nav class="navbar navbar-default">
	    <div class="navbar-header">
	      <a class="navbar-brand catalize-logo" href="#"><img src="{{ URL::asset('images/22Catalyze.png')  }}"></a>
	    </div>
        <div class="head-menu pull-right">
    	    <ul class="nav navbar-nav" style="margin-right: 150px">
    	      	<li class="active"><a href="/">Home</a></li>
    		    <li><a href="/about">About</a></li>
    		    <li><a href="/works">Works</a></li>
    		    <li><a href="/services">Services</a></li>
    	    </ul>
    	    <ul class="nav navbar-nav">
    	      	<li><a href="/"><i class="fa fa-facebook"></i></a></li>
    	      	<li><a href="/"><i class="fa fa-twitter"></i></a></li>
    	      	<li><a href="/"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="/"><i class="fa fa-search"></i></a></li>
    	    </ul>
        </div>
	</nav>
</div>

<style type="text/css">
	@import url(http://fonts.googleapis.com/css?family=Montserrat:400,700);
	header{
		padding-top: 10px;
		padding-bottom: 10px;
		background-color: #ffffff;	
		border-bottom: 1px solid #c2c2c2;
	}
	.nav ul .navbar-nav {
		color: #636363;
		font-size: 15px;
	}

	.navbar-default {
		margin-bottom: 0;
		border: none;
	}

    .navbar-default .navbar-nav > .active > a {
        color: #fc8e2e;
        background-color: #ffffff;
    }

    .navbar-default .navbar-nav > .active > a:hover {
        background-color: #c2c2c2;
    }

	.submenu {
		text-align: center;
	}
	.catalize-logo {
		padding-top: 5px;
		padding-bottom: 5px;
	}
	.catalize-logo img {
		height: 40px;
        width: auto;
	}

	footer{
		position: relative;
		display: block;
	}
	.grey {
		background-color: #e8e8e8;
		color: #898989;

	}
	
	body {
		background-color: #ffffff;
		font-family: 'Montserrat';
	}

	h3 {
		color: #252525;
	}

	.submenu {
		width: 100%;
		display: inline-block;
	}
	.submenu ul.navbar-nav{
		float: none;
		display: inline-block;
		font-size: 13px;
	}

	.owl-wrap a {
        display: inline-block;
    }
    .owl-carousel .owl-item a img {
        max-height: 110px;
        max-width: 200px;
        width: auto;
        padding-right: 60px;
    }
    .owl-carousel .oc-item {
        height: auto;
        display: flex;
        margin-bottom: 50px;
    }
    .owl-carousel .oc-item a {
        align-self: center;
    }
    .owl-carousel .oc-item-post a img {
        /*max-height: 210px;
        max-width: 330px;*/
        width: auto;
    }
    .owl-carousel .oc-item-post {
        height: 280px;
        width: 100%;
        padding-right: 60px;
        margin-bottom: 40px;
        /*display: flex;*/
    }
    .owl-carousel .oc-item-post .ipost {
    	width: 100%;
    }
    .owl-carousel .oc-item-post + .oc-item-post {
    	margin-top: 15px;
    }
    .owl-carousel .oc-item-post .ipost .entry-image a {
    	width: 100%;
    	height: 210px;
    	display: flex;
    	overflow: hidden;
    }
    .owl-carousel .oc-item-post .ipost .entry-title a {
    	display: block;
    }
    /*.owl-carousel .oc-item-post a img*/
    .owl-carousel .oc-item-post .ipost .entry-image a img {
    	min-width: 350px;
    	min-height: 210px;
    	max-width: initial;
    	max-height: initial;
    	/*height: */
    }

    .featured-articles {
        width: 100%;
        height: 320px;
        display: block;
    }
    .featured-articles .entry-image a {
        display: inline-block;
        width: 100%;
        height: 210px;
        overflow: hidden;
    }
    .featured-articles .entry-image a img {
        min-height: 210px;
        width: 330px;
    }
    .featured-articles .entry-title a {
        font-size: 20px;
        color: #333;
    }

    .container-posts {
    	padding-top: 60px;
    	padding-bottom: 60px;
        background-color: #F5F5F5;
        /*width: 1080px;*/
    }

    .row {
        margin-left: 0px;
        margin-right: 0px;
    }
    .posts-carousel {
        /*margin-left: 86px;*/
    }

    #twitter-widget-0 { 
      width: 330px !important;
      height: 580px !important;
    }

    .entry-meta {
        padding-top: 20px;
        padding-bottom: 20px;
        font-family: "Open Sans";
        font-size: 12px;
        color: #636363;
    }

    .entry-title {
        margin-bottom: 40px;
    }
    .entry-title a{
        font-size: 18px;
        font-weight: bold;
        color: #252525;
        line-height: 1.4;
    }

    .owl-carousel .text-only {
        border-top: 1px solid #c2c2c2;
        border-bottom: 1px solid #c2c2c2;
    }

    .owl-carousel .text-only .entry-title a{
        font-size: 18px;
        font-weight: bold;
        color: #fc8e2e;
        line-height: 1.4;
    }

    ..owl-carousel .text-only .entry-content p{
        font-family: "Open Sans";
        font-size: 13px;
        font-weight: bold;
        color: #636363;
        line-height: 1.5;
    }
    
</style>