<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
	App\Post;


class HomeController extends Controller
{
    public function index()
    {
    	// load all articles / posts to be displayed on slider
        $posts = Post::all()->toArray();

        return view('pages.home', compact('posts'));
    }

    public function show($id)
    {
        $post = Post::find($id);

        return view('pages.detail', compact('post', 'id'));
    }
}
